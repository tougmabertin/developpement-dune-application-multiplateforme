﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.IO;
using GATHERMAN.Bd;

[assembly: Xamarin.Forms.Dependency(typeof(Bd))]
namespace GATHERMAN.Bd
{
   public  class Bd : IBd
    {
     public  SQLiteConnection getConnection()
        {
           var db = "DataBaseCommercants";
            var dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), db);
                return  new SQLiteConnection(dbPath);
        }
    }
}
