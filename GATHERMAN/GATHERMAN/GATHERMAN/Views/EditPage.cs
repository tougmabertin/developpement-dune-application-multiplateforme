﻿using GATHERMAN.Droid.Models;
using GATHERMAN.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace GATHERMAN.Views
{
    public class EditPage : ContentPage
    {

        private Entry nom, prenom, date, lieu, email, telephone, adresse, revenu,id;
    
        private ListView listView;
        private Commercant commercant; 
        private MockDataStore DataStore = new MockDataStore();
        public EditPage()
        {

            StackLayout stackLayout = new StackLayout();

            listView = new ListView();

            listView.ItemsSource = DataStore.GetCommercantsAsync().Result;

            listView.ItemSelected += commercantSelected;


            stackLayout.Children.Add(listView);

            id = new Entry();
            id.Placeholder = "ID";
            id.IsVisible = false;
            nom = new Entry();
            nom.Keyboard = Keyboard.Text;
            nom.Placeholder = "nom";
            stackLayout.Children.Add(nom);

            prenom = new Entry();
            prenom.Keyboard = Keyboard.Text;
            prenom.Placeholder = "prenom";
            stackLayout.Children.Add(prenom);

            date = new Entry();
            date.Keyboard = Keyboard.Text;
            date.Placeholder = "date";
            stackLayout.Children.Add(date);


            lieu = new Entry();
            lieu.Keyboard = Keyboard.Text;
            lieu.Placeholder = " lieu";
            stackLayout.Children.Add(lieu);



            telephone = new Entry();
            telephone.Keyboard = Keyboard.Text;
            telephone.Placeholder = " telephone";
            stackLayout.Children.Add(telephone);



            email = new Entry();
 
            email.Keyboard = Keyboard.Text;
            email.Placeholder = "email";
            stackLayout.Children.Add(email);
            adresse = new Entry();
            adresse.Keyboard = Keyboard.Text;
            adresse.Placeholder = "adresse";
            stackLayout.Children.Add(adresse);

            revenu = new Entry();
            revenu.Keyboard = Keyboard.Text;
            revenu.Placeholder = "revenu";
            stackLayout.Children.Add(revenu);

            Button save = new Button();
            save.Text = "enregistrer";
            save.Clicked += Save_Clicked;
            stackLayout.Children.Add(save);
            Content = stackLayout;
        }


        async void Save_Clicked(object sender, EventArgs e)
        {
            Commercant commercant = new Commercant
            {
                Nom = nom.Text,
                Prenom = prenom.Text,
                Email = email.Text,
                Date_Naissance = DateTime.Parse(date.Text),
                Lieu_Naissance = lieu.Text,
                Telephone = telephone.Text,
                Photo=" ",
                URL_Photo= "",
                Adresse = adresse.Text,
                Revenu_Journalier = Convert.ToDouble(revenu.Text)


            };


            await DataStore.UpdateCommercantAsync(commercant);
            // MessagingCenter.Send(this, "AddItem", Commercant);
            await Navigation.PopModalAsync();
        }
        private void commercantSelected(object sender, SelectedItemChangedEventArgs e)
        {
            commercant = e.SelectedItem as Commercant;


            nom.Text = commercant.Nom;
            prenom.Text = commercant.Prenom;
            date.Text = commercant.Date_Naissance.ToString();
            email.Text = commercant.Email;
            telephone.Text = commercant.Telephone;
            revenu.Text = commercant.Revenu_Journalier.ToString();
            adresse.Text = commercant.Adresse;

        }

    }
}