﻿using GATHERMAN.Droid.Models;
using GATHERMAN.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace GATHERMAN.Views
{
    public class DeleteCommercant : ContentPage
    {
        private ListView listView;
        private Commercant commercant;
        private MockDataStore DataStore = new MockDataStore();
        public DeleteCommercant()
        {
            StackLayout stackLayout = new StackLayout();

            listView = new ListView();

            listView.ItemsSource = DataStore.GetCommercantsAsync().Result;

            listView.ItemSelected += commercantSelected;
            stackLayout.Children.Add(listView);

            Button delete = new Button();
            delete.Text = "supprimer";
            delete.Clicked += Save_Clicked;
            stackLayout.Children.Add(delete);
            Content = stackLayout;
        }


        private void commercantSelected(object sender, SelectedItemChangedEventArgs e)
        {
            commercant = e.SelectedItem as Commercant;

        }

        async void Save_Clicked(object sender, EventArgs e)
        {
           
            await DataStore.DeleteCommercantAsync(commercant.ID_Commercant);
            // MessagingCenter.Send(this, "AddItem", Commercant);
            await Navigation.PopModalAsync();
        }
    }
}