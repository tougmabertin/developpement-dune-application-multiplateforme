﻿using GATHERMAN.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace GATHERMAN.Views
{
    public class GetAllCommercants : ContentPage
    {
        private ListView listView;
        private MockDataStore DataStore = new MockDataStore();
        public GetAllCommercants()
        {

            listView = new ListView();

            StackLayout stackLayout = new StackLayout();
            listView.ItemsSource = DataStore.GetCommercantsAsync().Result;

            stackLayout.Children.Add(listView);
            Content = stackLayout;
        }
    }
}