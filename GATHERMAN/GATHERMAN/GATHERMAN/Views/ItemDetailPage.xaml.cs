﻿using System;
using System.ComponentModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GATHERMAN.Models;
using GATHERMAN.ViewModels;
using GATHERMAN.Droid.Models;
using GATHERMAN.Services;

namespace GATHERMAN.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class ItemDetailPage : ContentPage
    {
     
        ItemDetailViewModel viewModel;
        public MockDataStore DataStore = new MockDataStore();
        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        public ItemDetailPage()
        {
            InitializeComponent();

            var Commercant = new Commercant
{
                Nom = "Commercant 1",
                Prenom = "This is an Commercant description."
            };

            viewModel = new ItemDetailViewModel(Commercant);
            BindingContext = viewModel;
        }
      
       

       





    }
}