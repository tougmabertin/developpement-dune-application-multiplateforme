﻿using System;

using System.ComponentModel;
using Xamarin.Forms;
using GATHERMAN.Droid.Models;
using Plugin.FilePicker;
using System.IO;

namespace GATHERMAN.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class NewItemPage : ContentPage
    {




        //public Commercant It { get; set; }
        public Commercant Commercant { get; set; }

        public NewItemPage()
        {
            InitializeComponent();


            
            
            Commercant = new Commercant
            {
                Nom = string.Empty,
                Prenom = string.Empty,
                Adresse = string.Empty,
                Date_Naissance = new DateTime(),
                Lieu_Naissance = string.Empty,
                Email = string.Empty,
                Photo = string.Empty,
                URL_Photo = string.Empty,
                Revenu_Journalier = new double()

            };

            BindingContext = this;
        }
        private async void Button_Clicked(object sender, EventArgs e)
        {
            var file = await CrossFilePicker.Current.PickFile();

            if (file != null)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path=  Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), fileName);
               path= file.FilePath;
               
               // file.SaveAs(path);
               
       
            }
        }
    


    async void Save_Clicked(object sender, EventArgs e)
        {
           
    


            MessagingCenter.Send(this, "AddItem", Commercant);
            await Navigation.PopModalAsync();
        }
      
        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
       
    }
}