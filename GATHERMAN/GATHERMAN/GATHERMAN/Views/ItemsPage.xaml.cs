﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GATHERMAN.Models;
using GATHERMAN.Views;
using GATHERMAN.ViewModels;
using GATHERMAN.Droid.Models;

namespace GATHERMAN.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;
        //ListView listView;
        public ItemsPage()
        {

            InitializeComponent();
            BindingContext = viewModel = new ItemsViewModel();

        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var Commercant = args.SelectedItem as Commercant;
            if (Commercant == null)
                return;

            await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(Commercant)));

            // Manually deselect Commercant.
            ItemsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));
        }
        async void Edit_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new EditPage());
        }

        async void Delete_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new DeleteCommercant());
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.DataStore.GetCommercantsAsync(true) == null)
                viewModel.LoadItemsCommand.Execute(null);
        }
    }
}