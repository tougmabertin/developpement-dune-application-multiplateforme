﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GATHERMAN.Services
{
    public interface IDataStore<T>
    {
        Task<bool> AddCommercantAsync(T commercant);
        Task<bool> UpdateCommercantAsync(T commercant);
        Task<bool> DeleteCommercantAsync(int id);
        Task<T> GetCommercantAsync(int id);
        Task<IEnumerable<T>> GetCommercantsAsync(bool forceRefresh = false);
    }
}
