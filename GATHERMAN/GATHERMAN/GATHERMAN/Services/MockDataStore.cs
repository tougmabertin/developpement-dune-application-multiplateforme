﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GATHERMAN.Bd;
using GATHERMAN.Droid.Models;
using GATHERMAN.Models;
using Newtonsoft.Json;
using SQLite;
using Xamarin.Forms;

namespace GATHERMAN.Services
{
    public class MockDataStore : IDataStore<Commercant>
    {
        public SQLiteConnection connexion = DependencyService.Get<IBd>().getConnection();
        List<Commercant> commercants;
        static string uri = "https://github.com/images/error/octocat_happy.gif";
        public MockDataStore()
        {
            commercants = new List<Commercant>();
            var mockCommercants = new List<Commercant>
            {
                 new Commercant { ID_Commercant =1, Nom = "TOUGMA", Prenom="bertin is an item description." },
                new Commercant { ID_Commercant =1, Nom = "First item", Prenom="This is an item description." },
                new Commercant { ID_Commercant =2, Nom = "Second item", Prenom="This is an item description." },
                new Commercant { ID_Commercant =3, Nom = "Third item", Prenom="This is an item description." },
                new Commercant { ID_Commercant =4, Nom = "Fourth item", Prenom="This is an item description." },
                new Commercant { ID_Commercant =5, Nom = "Fifth item", Prenom="Azouman is an item description." },
                new Commercant { ID_Commercant =6, Nom = "Sixth item", Prenom="This is an item description." },
            };
            connexion.CreateTable<Commercant>();
            // connexion.DeleteAll<Commercant>();
            foreach (var commercant in mockCommercants)
            {
                commercants.Add(commercant);
            }
        }

        public async Task<bool> AddCommercantAsync(Commercant commercant)
        {
            // connexion a l'API web
            var json = JsonConvert.SerializeObject(commercant);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            var result = await client.PostAsync(uri, content);

            // ajout de d'un commercant
            commercants.Add(commercant);


            //var indexMax = connexion.Table<Commercant>().OrderByDescending(c => c.ID_Commercant).FirstOrDefault();
            connexion.Insert(commercant);
            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateCommercantAsync(Commercant commercant)
        {
            // mise à jour d'un commercant
            var oldItem = commercants.Where((Commercant arg) => arg.ID_Commercant == commercant.ID_Commercant).FirstOrDefault();
            commercants.Remove(oldItem);
            commercants.Add(commercant);

            /*int idvalue = Convert.ToInt32(commercant.ID_Commercant); ;
            var comer = (from values in connexion.Table<Commercant>()
                         where values.ID_Commercant == idvalue
                         select values).Single();
            if(comer!=null)
            connexion.Update(commercant);*/

            //Partie API
           var json = JsonConvert.SerializeObject(commercant);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            var result = await client.PutAsync(string.Concat(uri, commercant.ID_Commercant), content);
            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteCommercantAsync(int ID_Commercant)
        {
            //connexion a l'API
            HttpClient client = new HttpClient();
            var result = client.DeleteAsync(string.Concat(uri, ID_Commercant));


            // suppression d'un commercant


            var oldcommercant = commercants.Where((Commercant arg) => arg.ID_Commercant == ID_Commercant).FirstOrDefault();
            commercants.Remove(oldcommercant);

            
           // connexion.Table<Commercant>().Delete(x => x.ID_Commercant == ID_Commercant);
            return await Task.FromResult(true);
        }

        public async Task<Commercant> GetCommercantAsync(int ID_Commercant)
        {
            // client = new HttpClient();
            //var result = await client.GetAsync(string.Concat(uri, ID_Commercant));
            return await Task.FromResult(connexion.Table<Commercant>().FirstOrDefault(s => s.ID_Commercant == ID_Commercant));
        }

        public async Task<IEnumerable<Commercant>> GetCommercantsAsync(bool forceRefresh = false)
        {
            // Appel Api
           HttpClient client = new HttpClient();
            return await Task.FromResult(commercants);
        }


    }
}