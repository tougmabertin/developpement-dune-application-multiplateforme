﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using GATHERMAN.Models;
using GATHERMAN.Views;
using GATHERMAN.Droid.Models;

namespace GATHERMAN.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableCollection<Commercant> Commercants { get; set; }

        public Command LoadItemsCommand { get; set; }

        public ItemsViewModel()
        {
            Title = "Agent";
            var commercants = DataStore.GetCommercantsAsync(true).Result;
            Commercants = new ObservableCollection<Commercant>(commercants);
            //Commercants = new ObservableCollection<Commercant>;
            LoadItemsCommand = new Command(async () => await ExecuteLoadCommercantsCommand());

            MessagingCenter.Subscribe<NewItemPage, Commercant>(this, "AddItem", async (obj, Commercant) =>
            {
                var newCommercant = Commercant as Commercant;

                // Commercants.Add(newCommercant);
                await DataStore.AddCommercantAsync(newCommercant);
            });
        }

        async Task ExecuteLoadCommercantsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {

                //this.Commercants.Clear();
                var Commercants = await DataStore.GetCommercantsAsync(true);
                foreach (var Commercant in Commercants)
                {
                    await this.DataStore.AddCommercantAsync(Commercant);
                    // this.Commercants.Add(Commercant);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}