﻿using System;
using GATHERMAN.Droid.Models;
using GATHERMAN.Models;
using GATHERMAN.Views;
using Xamarin.Forms;

namespace GATHERMAN.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Commercant Commercant { get; set; }
        public ItemDetailViewModel(Commercant Commercant = null)
        {
            Title = Commercant?.Nom;
            this.Commercant = Commercant;
        }
      
    }
}
