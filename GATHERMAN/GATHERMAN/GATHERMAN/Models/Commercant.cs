﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using Xamarin.Forms;

namespace GATHERMAN.Droid.Models
{
    [Table("Commercant")]
    public class Commercant
    {
        [PrimaryKey, AutoIncrement]
        public int ID_Commercant { get; set; }
        
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public System.DateTime Date_Naissance { get; set; }
        public string Lieu_Naissance { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public string URL_Photo { get; set; }
        public string Adresse { get; set; }
        public double Revenu_Journalier { get; set; }
        public override string ToString()
        {return this.Nom + "(" + this.Adresse + ")";}
    }
}