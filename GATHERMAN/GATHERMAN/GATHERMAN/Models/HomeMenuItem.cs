﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GATHERMAN.Models
{
    public enum MenuItemType
    {
        Agent,
        Commercants,
        Parametre
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
